#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>
int main ()
{
  pid_t pid;
  pid = fork ();
  /* fork () returns negative number in case it fails to fork a child process.
   * Upon successful fork, a child process is created which is identical to
   * the parent process. Both the parent and child process starts executing the 
   * instructions which are followed by the fork () call.
   *
   * When the child process is running, fork () returns value 0 and when parent
   * is running it returns pid of the child process, which can be used to
   * distinguish between child and parent process. */

  if (pid < 0)
  {
    fprintf (stderr, "Fork Failed");
    return 1;
  }
  else if (pid == 0)  // Child process
    execlp ("/bin/ls", "ls", NULL);
  else    // Parent process
  {
    wait (NULL);
    printf ("Child Complete\n");
  }
  return 0;
}
