#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>

/* Allocates and attaches shared memory and writes a string to it.
 * Which is then read by the client and updated to acknowledge
 * the same. */
void main ()
{
  int i;
  key_t key;                 // Key for the shared memory.
  int shmid;                 // Id of shared memory.
  char *shm;                 // Attached data with the shared memory.
  const char *msg = "SECRET";
  int segment_size = 4 * 1024;    // Equal to page size.

  key = 2341;

  /* Allocate shared memory segment. */
  shmid = shmget (key, segment_size, IPC_CREAT | 0666);
  if (shmid < 0)
  {
    printf ("ERROR SHMGET\n");
    return;
  }

  /* Attach the memory segment to this process (server). */
  shm = shmat (shmid, 0, 0);

  /* Copy data to the shared memory. */
  strcpy (shm, msg);
 
  printf ("Data written to the shared memory %s\n", shm);

  /* Wait till the client updates the shared data for
   * acknowledgement.*/
  while (*shm != '*');

  /* Detach the shared memory segment. */
  shmdt (shm);

  /* Deallocate the shared memory segment. */
  shmctl (shmid, IPC_RMID, 0);
}
