#include <bits/stdc++.h>
using namespace std;

/*--------------------LRU page replacement algorithm---------------------------
 * Victim page in the cache is the one which has been least recently used.
 * Implementation Details: As soon as a page is referenced, it is pushed to the
 * front of a doubly-linked list. If the page is already present, then move it
 * to the front of the list, i.e. first delete it and then push it to the front. 
 * The last element of the list becomes the new victim page for replacement.
 * Now, the deletion can be done in O(1) by keeping record of the pointer of
 * each page in the list using a hash table. To keep things simple, we can use
 * an array instead of a hash table if the pages are numbered up to a fixed 
 * value. */

#define MAX_PAGE 1000       /* Maximum no. of pages; numbered (0..MAX_PAGE-1) */
#define FRAMES_COUNT 3      /* No. of frames allocated to the process. */

void lru (vector<int>& mem_ref)
{
  list<int> lru_list;
  vector<list<int>::iterator> position (MAX_PAGE, lru_list.end ());     
  int page_faults = 0, sz = 0;
  for (auto i = mem_ref.begin (); i != mem_ref.end (); i++)
  {
    if (position[*i] != lru_list.end ())     /* Cache hit. */
    {
      lru_list.erase (position[*i]);
      lru_list.push_front (*i);
      position[*i] = lru_list.begin ();
      continue;
    }
    if (sz == FRAMES_COUNT)       /* Is the cache full? */
    {
      cout << "Evicting page " << lru_list.back () << endl;
      position[lru_list.back ()] = lru_list.end ();
      lru_list.pop_back ();
      sz--;
    }
    page_faults++;
    sz++;
    lru_list.push_front (*i);
    position[*i] = lru_list.begin ();
  }
  cout << "No. of page faults = " << page_faults << endl;
}

void fifo (vector<int>& mem_ref)
{
  queue<int> q;
  unordered_map<int, bool> present;
  int sz = 0, page_faults = 0;
  for (auto i = mem_ref.begin (); i != mem_ref.end (); i++)
  {
    if (present.find ((*i)) != present.end () && present[(*i)])  /* Cache hit.*/       
      continue;
    if (sz == FRAMES_COUNT)
    {
      cout << "Evicting page " << q.front () << endl;
      present[q.front ()] = false;
      q.pop ();
      sz--;
    }
    q.push ((*i));
    present[(*i)] = true;
    page_faults++;
    sz++;
  }
  cout << "No. of page faults = " << page_faults << endl;
}

int main ()
{
  int n;
  cout << "Enter the size of the memory reference string\n";
  cin >> n;
  cout << "Enter the memory reference string\n";
  vector<int> mem_ref (n);
  for (int i = 0; i < n; i++)
    cin >> mem_ref[i];
  lru (mem_ref);
  fifo (mem_ref);
  return 0;
}
