#include "process.h"
#include "scheduler.h"
using namespace std;
#ifndef PRI_H
#define PRI_H

/* Non premptive priority scheduler. */
class np_priority_scheduler : public scheduler
{
public:

  np_priority_scheduler (const process& idle_process) : scheduler::
                                                        scheduler (idle_process)
  {}

  /* Inserts process p into the ready_queue, on basis of priority. */
	void insert (const process& p)
	{
		for (auto i = ready_queue.begin (); i != ready_queue.end (); i++)
		{
			if ((*i).priority > p.priority)
			{
				ready_queue.insert (i, p);
				return;
			}
		}
		ready_queue.push_back (p);
	}

  /* Non-premptive case: no other process can execute in between the execution 
   * of the current process. */
  void schedule ()
  {
    while (!job_queue.empty () || !ready_queue.empty ())
    {
      /* While there is no process to schedule, pass time. */
      while (ready_queue.empty ())
        timer_interrupt ();

      current_p = ready_queue.front ();
      ready_queue.erase (ready_queue.begin ());
      waiting_time[current_p.pid - 1] = timer_ticks - current_p.arrival_time;
      turn_around_time[current_p.pid - 1] = waiting_time[current_p.pid - 1] + 
                                            current_p.burst_time;
      current_p.execute ();
      /* Call timer_interrupt () burst_time times. */
      for (int i = 0; i < current_p.burst_time; i++)
        timer_interrupt ();
    }
  }
  
};

/* Premptive priority scheduler. */
class p_priority_scheduler : public scheduler
{
protected:

  /* Extends timer_interrupt () of base class. It now also checks if there 
   * exists a process of higher priority in the ready_queue or not. If it 
   * exists, prempt the current process and run the higher priority process. */
  void timer_interrupt ()
  {
    scheduler::timer_interrupt ();
    if (!ready_queue.empty () && ready_queue.front ().priority < current_p.priority)
    {
     current_p = ready_queue.front (); 
     return;
    }
  }
public:

  p_priority_scheduler (const process& idle_process) : scheduler::
                                                       scheduler (idle_process)
  {}

  /* Inserts process p into the ready_queue, on basis of priority. */
	void insert (const process& p)
	{
		for (auto i = ready_queue.begin (); i != ready_queue.end (); i++)
		{
			if ((*i).priority > p.priority)
			{
				ready_queue.insert (i, p);
				return;
			}
		}
		ready_queue.push_back (p);
	}

  void schedule ()
  {
    while (!job_queue.empty () || !ready_queue.empty ())
    {
      /* While there is no process to schedule, pass time. */
      while (ready_queue.empty ())
        timer_interrupt ();

      current_p = ready_queue.front ();
      ready_queue.erase (ready_queue.begin ());
      if (turn_around_time[current_p.pid - 1] == 0) // Has the process executed
                                                    // before?
        waiting_time[current_p.pid - 1] = timer_ticks - current_p.arrival_time;
      else
        waiting_time[current_p.pid - 1] += timer_ticks
                                           - turn_around_time[current_p.pid - 1] 
                                           - current_p.arrival_time;
      turn_around_time[current_p.pid - 1] = timer_ticks
                                            - current_p.arrival_time;
      
      current_p.execute ();
      process p = current_p;

      int i;
      /* Call timer_interrupt () burst_time times. */
      for (i = 1; i <= p.burst_time; i++)
      {
        turn_around_time[current_p.pid - 1]++;
        timer_interrupt ();
        if (current_p.pid != p.pid)  // Prempt the current process. 
          break;
      }

      if (i != p.burst_time + 1)
      {
        p.burst_time -= i;
        insert (p);
      }
    }
  }
};
#endif
