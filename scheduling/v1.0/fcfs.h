#include "process.h"
#include "scheduler.h"
using namespace std;
#ifndef FCFS_H
#define FCFS_H

class fcfs_scheduler : public scheduler
{
public:
	void insert (const process& p)
	{
		ready_queue.push_back (p);
	}
};

#endif
