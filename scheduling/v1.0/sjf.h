#include "process.h"
#include "scheduler.h"
using namespace std;
#ifndef SJF
#define SJF
class shortest_job_first : public scheduler
{
public:
	void insert (const process& p)
	{
		for (auto i = ready_queue.begin (); i != ready_queue.end (); i++)
		{
			if ((*i).burst_time > p.burst_time)
			{
				ready_queue.insert (i, p);
				return;
			}
		}
		ready_queue.push_back (p);
	}

};
#endif
