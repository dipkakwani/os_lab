#include <bits/stdc++.h>
using namespace std;
#ifndef PROCESS_H
#define PROCESS_H

enum status
{
	RUNNING, READY, BLOCKED
};

class process
{
public:
	unsigned int pid;						// Process identifier.
	void (*task) ();						// Task to be performed by the process.
	string name;								// Name of the prcess.
	time_t arrival_time;				// Time of arrival of the process.
	int priority;								// Priority of the process. If not valid, set to PRI_UND.
	status s;							      // Current status of the process.
	unsigned long burst_time;		// Execution time. 
	void execute ()
	{
		task ();
	}
	process (string nm, unsigned int id, int pri, time_t a_time, 
           unsigned long b_time, void (*t) ())	
	{
		name = nm;
		pid = id;
		priority = pri;
    arrival_time = a_time;
		burst_time = b_time;
		task = t;
	}
};
#endif
