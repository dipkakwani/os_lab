#include <bits/stdc++.h>
using namespace std;
typedef vector<int> vi;
typedef vector<vector<int>> vvi;

bool safe_state (vi& available, vvi& need, vvi& allocation)
{
	int m = available.size ();		// Number of resources.
	int n = need.size ();					// Number of processes.
	vector<int> work (available);	// Currently available resourses with count of 
                                // instances.
	vector<bool> finish (n);			// Has process i finished its execution?
	int count = 0;								// Number of processes which have finished 
                                // execution.
	int i, j;

	/* Greedy approach : Pick the first process whose needs can be satisfied with 
   * the currently available instances of resources. Repeat this until either no
   * process remains or none of the process satisfy the constraint. In the 
   * former case, system is said to be in safe state and in the latter case, it
   * is not a safe state. */
	
	while (1)
	{
		for (i = 0; i < n; i++)
		{
			if (!finish[i])
			{
				for (j = 0; j < m; j++)
				{
					if (need[i][j] > work[j])
						break;
				}
				if (j == m)
				{
					count++;
					finish[i] = true;
					for (j = 0; j < m; j++)
					{
						work[j] += allocation[i][j];
					}
					break;
				}
			}
		}
		if (i == n)
		{
			if (count == n)
				return true;
			return false;
		}
	}
}

bool resource_request (vi& available, vvi& need, vvi& allocation, vvi& max,
                       vi& request, int id)
{
	int m = available.size ();
	int n = need.size ();

	for (int i = 0; i < m; i++)
	{
		if (request[i] > need[id][i])
		{
			cout << "Error: Requested more than needed!\n";
			return false;
		}
    if (request[i] > available[i])
    {
      cout << "Requested resource not available!\n";
      return false;
    }
	}
  /* Pretend to allocate the resources. If the current state is a safe state, 
   * then the transaction is complete else, revert back to the original 
   * state. */
	for (int i = 0; i < m; i++)
  {
    available[i] -= request[i];
    allocation[id][i] += request[i];
    need[id][i] -= request[i];
  }

	if (!safe_state (available, need, allocation))
  {
    // Revert back to the original state.
  	for (int i = 0; i < m; i++)
    {
      available[i] += request[i];
      allocation[id][i] -= request[i];
      need[id][i] += request[i];
    }   
    return false;
  }
  return true;  
}

int main ()
{
	int n, m, x;
	cout << "Enter number of processes\n";
	cin >> n;
	cout << "Enter number of resources\n";
	cin >> m;
	vector<int> available (m);
	cout << "Enter number of instances of each resource\n";
	for (int k = 0; k < m; k++)
		cin >> available[k];

	vector<vector<int>> allocation (n, vector<int> (m));
	vector<vector<int>> max (n, vector<int> (m));
	vector<vector<int>> need (n, vector<int> (m));
  vi request (m);

	cout << "Enter currently allocated resources to each process\n";
	for (int i = 0; i < n; i++)
  {
		for (int j = 0; j < m; j++)
    {
			cin >> allocation[i][j];
      available[j] -= allocation[i][j];
    }
  }

	cout << "Enter maximum resources needed of each process\n";
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
		{
			cin >> max[i][j];
			need[i][j] = max[i][j] - allocation[i][j];
		}
	}
	if (safe_state (available, need, allocation))
		cout << "Safe state\n";
	else
		cout << "Not a safe state\n";

  cout << "Enter a request (id, request vector)\n";
  cin >> x;
  for (int i = 0; i < m; i++)
    cin >> request [i];
  if (resource_request (available, need, allocation, max, request, x)) 
    cout << "Request processed successfully.\n";
  else
    cout << "Request can not be processed; not a safe state\n";
	return 0;
}
